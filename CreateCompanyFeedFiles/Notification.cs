﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreateCompanyFeedFiles
{
    public class Notification
    {
        public string ToList { get; set; }
        public string CcList { get; set; }
        public string BccList { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string MessageSubject { get; set; }
        public string ToName { get; set; }
        public string FromName { get; set; }
        public string FromEmail { get; set; }
        public Attachments Attachments { get; set; }
    }
}
