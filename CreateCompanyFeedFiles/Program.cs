﻿using Npgsql;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Serilog;
using Microsoft.Extensions.Configuration;
using Serilog.Core;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using System.Text.Json;

namespace CreateCompanyFeedFiles
{
    enum Environment
    {
        prod,
        staging,
        dev
    }

    class Program
    {
        static bool                sendEmail    = false;
        static string              emailMessage = string.Empty;
        static HttpResponseMessage response;

        static async Task Main(string[] args)
        {
            string env          = SetupEnvironment(args);

            ConfigValues configValues = GetConfigValues(env);
            Logger log = CreateLogger(configValues.SeriLogEventLevel);
            try
            {
                //Timing info for the log
                DateTime startTime = DateTime.Now;
                log.Information("========================================>");
                log.Information("Run Started at : " + startTime.ToString());

                //Setup database for reading
                using NpgsqlConnection dbConnection = new NpgsqlConnection(configValues.DbConnString);
                string query = @"SELECT DISTINCT feed_id from web.branch_feeds ";

                if (!string.IsNullOrWhiteSpace(configValues.ExcludeWebsiteFeeds))
                {
                    query += "where feed_id not in ("  + configValues.ExcludeWebsiteFeeds + ")";
                }

                using var cmd = new NpgsqlCommand(query, dbConnection);

                dbConnection.Open();
                using var rdr = cmd.ExecuteReader();

                //HTTP client
                using HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(configValues.BaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

                int nrOfRecords = 0;
                //Processing of feeds
                while (rdr.Read())
                {
                    ProcessFeed(rdr.GetGuid(0), log, client, configValues);
                    nrOfRecords++;
                }

                dbConnection.Close();

                //Timing info for the log
                DateTime endTime = DateTime.Now;
                TimeSpan runningDurarion = endTime.Subtract(startTime);
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    runningDurarion.Hours, runningDurarion.Minutes, runningDurarion.Seconds, runningDurarion.Milliseconds / 10);

                log.Information("Run Ended at : " + startTime.ToString());
                log.Information(nrOfRecords.ToString() + " feeds processed in " + elapsedTime);

                if (sendEmail)
                    response = await SendEmail(emailMessage, configValues.EmailRecipients, configValues.EmailPostUri, log);
            }

            catch (Exception ex)
            {
                log.Error(ex, ex.Message);
                var response = await SendEmail(ex.Message, configValues.EmailRecipients, configValues.EmailPostUri, log);
            }
        }

        static async void ProcessFeed(Guid feedId, Logger log, HttpClient client, ConfigValues configValues)
        {
            log.Information("----------------------");
            log.Information("Started processing of feed id : " + feedId.ToString() + " at " + DateTime.Now.ToString());
            string requestUri = "Feed?feedId=" + feedId.ToString() + "&useftp=" + configValues.UseFtp 
                + "&returnFeed=" + configValues.ReturnFeed + "&xmlBaseFolder=" + configValues.XmlBaseFolder;
            var responseTask = client.GetAsync(requestUri);
            responseTask.Wait();
            var result = responseTask.Result;

            if (result.IsSuccessStatusCode)
                log.Information("API success");
            else
            {
                log.Information("API failed");
                sendEmail = true;
                emailMessage += "API call failed for feedId: " + feedId.ToString() + System.Environment.NewLine;
            }

            log.Information("Ended   processing of feed id : " + feedId.ToString() + " at " + DateTime.Now.ToString());
        }
        static ConfigValues GetConfigValues(string env)
        {
            IConfiguration config = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json", true, true)
                    .Build();

            var configValues               = new ConfigValues();
            configValues.DbConnString      = config.GetSection("Environment").GetSection(env).GetSection("DatabaseConnection").Value;
            configValues.BaseURL           = config.GetSection("Environment").GetSection(env).GetSection("BaseUrl").Value;
            configValues.XmlBaseFolder     = config.GetSection("Environment").GetSection(env).GetSection("XmlBaseFolder").Value;
            configValues.UseFtp            = config.GetSection("ApiConfig").GetSection("UseFtp").Value;
            configValues.ReturnFeed        = config.GetSection("ApiConfig").GetSection("ReturnFeed").Value;
            configValues.SeriLogEventLevel = config.GetSection("ApiConfig").GetSection("SeriLogEventLevel").Value;
            configValues.EmailRecipients   = config.GetSection("ApiConfig").GetSection("EmailRecipients").Value;
            configValues.EmailPostUri      = config.GetSection("ApiConfig").GetSection("EmailPostUri").Value;
            var websiteFeeds               = config.GetSection("ExcludeWebsiteFeeds").GetChildren();

            //List of website feeds to exclude (all website feeds should be excluded)
            string listOfWebsiteFeeds      = string.Empty;

            foreach (var conf in websiteFeeds)
                listOfWebsiteFeeds += "'" + conf.Value + "',";

            configValues.ExcludeWebsiteFeeds = listOfWebsiteFeeds.Trim(',');

            return configValues;
        }

        static Logger CreateLogger(string logEventLevel)
        {
            Enum.TryParse(logEventLevel, out Serilog.Events.LogEventLevel seriLogEventLevel);

            Logger log = new LoggerConfiguration()
                    .WriteTo.Console()
                    .WriteTo.File("logs\\CreateCompanyFeedsLog.txt", seriLogEventLevel, rollingInterval: RollingInterval.Day)
                    .CreateLogger();

            return log;
        }

        static string SetupEnvironment(string[] args)
        {
            if (args.Length == 0)
                LogStartupErrorAndThrowException("Not a valid environment. Use one of prod/staging/dev.");

            string env = args[0].ToLower();

            if (!Enum.IsDefined(typeof(Environment), env))
            {
                LogStartupErrorAndThrowException(env + ": is not a valid environment. Use one of prod/staging/dev.");
            }

            return env;
        }

        static void LogStartupErrorAndThrowException(string msg)
        {
            Logger log = CreateLogger("Information");
            log.Error(msg);
            throw new Exception(msg);
        }

        static async Task<HttpResponseMessage> SendEmail(string message, string emailRecipients, string emailPostUri, Logger log)
        {

            BodyContent bodyContent = new BodyContent()
            {
                Notification = new Notification()
                {
                    ToList = emailRecipients,
                    CcList = "",
                    BccList = "",
                    Subject = "CreateCompanyFeedFiles.exe has encountered an error.",
                    Message = "CreateCompanyFeedFiles.exe has encountered an error. Please check the log files for more details. " + message,
                    MessageSubject = "",
                    ToName = "Support",
                    FromName = "OS Background Worker",
                    FromEmail = "webmaster@vmgsoftware.co.za",
                    Attachments = new Attachments()
                }
            };

            string json = JsonSerializer.Serialize(bodyContent);
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            using var client = new HttpClient();

            var response = await client.PostAsync(emailPostUri, data);

            return response;
        }
    }

    class ConfigValues
    {
        public string     DbConnString;
        public string     BaseURL;
        public string     UseFtp;
        public string     ReturnFeed;
        public string     SeriLogEventLevel;
        public string     ExcludeWebsiteFeeds;
        public string     EmailRecipients;
        public string     EmailPostUri;
        public string     XmlBaseFolder;
    }
}
